# vaultwarden

## Configure

as usual, setup your `.env` with:

```
SMTP_HOST=
SMTP_FROM=
SMTP_PORT=465
SMTP_SECURITY=force_tls
SMTP_USERNAME=
SMTP_PASSWORD=
SIGNUPS_ALLOWED=false
ADMIN_TOKEN=
POSTGRES_PASSWORD=
```

If you are in debug: `echo PROTOCOL=http >> .env`

Then go to the `/admin` url and enter your `ADMIN_TOKEN`


## Updating

```
docker compose down
docker compose pull
docker compose up -d
```
