# Collabora

https://www.collaboraoffice.com/

This configuration allows to run a Collbora instance to be used with Nextcloud (not tested with any other tool)

## Configure

Create a .env file containing the variables:
- domain
- server_name

For basic setup (1 Nextcloud and 1 Collabora):
```
domain=cloud.domain.com
server_name=collabora.domain.com
```

If you want to serve multiple Nextcloud instances with 1 Collabora instance:
```
domain=cloud.domain.com|cloud.domain2.com
server_name=collabora.domain.com
```

## Deploy

```bash
docker compose up -d
```

Check that `https://colabora.domain.com` is returing `OK`

In your Nextcloud instance, install the App `Nextcloud office`

The go in the settings > Nextcloud Office section and enter the collabora URL (value defined in `server_name` with https:// in front of it)

# Warning

Currently Collabora is not compatible with OnlyOffice (unless you apply this [workaround](https://lab.libreho.st/libre.sh/docker/nextcloud/-/merge_requests/12/diffs#diff-content-dc5ea057fcb8783df59ef97caf2fa02971550316))

